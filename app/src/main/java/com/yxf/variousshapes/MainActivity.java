package com.yxf.variousshapes;

import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    //1,定义GLSurfaceView对象,这个View提供了OpenGL ES的显示窗口
    private GLSurfaceView glSurfaceView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //2,创建GLSurfaceView对象
        glSurfaceView = new GLSurfaceView(this);
        //3,设置OpenGL ES版本为2.0
        glSurfaceView.setEGLContextClientVersion(2);
        //4,设置渲染器
        glSurfaceView.setRenderer(new MyRenderer(this));
        //5,设置GLSurfaceView为主窗口
        setContentView(glSurfaceView);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //6.1,当Activity暂停时暂停glSurfaceView
        glSurfaceView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //6.2,当Activity恢复时恢复glSurfaceView
        glSurfaceView.onResume();
    }
}

package com.yxf.variousshapes.shapes;

import android.graphics.Color;

import com.yxf.variousshapes.Point;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;


import static android.opengl.GLES20.*;

public class Square extends Shape {
    private float[] vertices;
    private FloatBuffer vertexData;
    private final int color = Color.rgb(255, 0, 0);

    public Square(Point center, float width, float height) {
        super(center, width, height);
        float left = center.x - width / 2;
        float right = center.x + width / 2;
        float top = center.y + height / 2;
        float bottom = center.y - height / 2;
        float red = Color.red(color) / 255f;
        float green = Color.green(color) / 255f;
        float blue = Color.blue(color) / 255f;
        vertices = new float[]{
                left, top, red, green, blue,
                left, bottom, red, green, blue,
                right, bottom, red, green, blue,
                right, top, red, green, blue,

        };
        vertexData = ByteBuffer.allocateDirect(vertices.length * BYTES_PER_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        vertexData.put(vertices);
    }

    @Override
    public void draw() {
        vertexData.position(0);
        glVertexAttribPointer(aPositionLocation, POSITION_COMPONENT_COUNT, GL_FLOAT,
                false, STRIDE, vertexData);
        vertexData.position(POSITION_COMPONENT_COUNT);
        glVertexAttribPointer(aColorLocation, COLOR_COMPONENT_COUNT, GL_FLOAT,
                false, STRIDE, vertexData);

        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    }
}

package com.yxf.variousshapes.shapes;

import android.graphics.Color;

import com.yxf.variousshapes.Point;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_TRIANGLE_FAN;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glVertexAttribPointer;

public class Star extends Shape {

    private final int angleCount;
    private final int pointCount;
    private final int size;
    private float[] vertices;
    private FloatBuffer vertexData;
    private final int centerColor = Color.rgb(255, 255, 255);
    private final int circumColor = Color.rgb(0, 0, 255);

    private final float rate;

    public Star(Point center, float size, int angleCount) {
        super(center, size, size);
        this.angleCount = angleCount;
        this.size = (int) size;
        pointCount = angleCount * 2 + 2;
        rate = getRate(angleCount);
        vertices = new float[(POSITION_COMPONENT_COUNT + COLOR_COMPONENT_COUNT) * pointCount];
        int i = 0;
        //中心
        vertices[i++] = center.x;
        vertices[i++] = center.y;
        vertices[i++] = Color.red(centerColor) / 255f;
        vertices[i++] = Color.green(centerColor) / 255f;
        vertices[i++] = Color.blue(centerColor) / 255f;

        while (i < vertices.length) {
            int index = i / (POSITION_COMPONENT_COUNT + COLOR_COMPONENT_COUNT) - 1;
            double angle = Math.PI * index / angleCount;
            if (index % 2 == 1) {
                vertices[i++] = center.x + (float) Math.cos(angle) * size / 2;
                vertices[i++] = center.y + (float) Math.sin(angle) * size / 2;
                vertices[i++] = Color.red(circumColor) / 255f;
                vertices[i++] = Color.green(circumColor) / 255f;
                vertices[i++] = Color.blue(circumColor) / 255f;
            } else {
                vertices[i++] = center.x + (float) Math.cos(angle) * size / 2 / rate;
                vertices[i++] = center.y + (float) Math.sin(angle) * size / 2 / rate;
                vertices[i++] = Color.red(circumColor) / 255f;
                vertices[i++] = Color.green(circumColor) / 255f;
                vertices[i++] = Color.blue(circumColor) / 255f;
            }
        }

        vertexData = ByteBuffer.allocateDirect(vertices.length * BYTES_PER_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        vertexData.put(vertices);
    }


    /**
     * 获得外径和内径的比值
     *
     * @param angleCount
     * @return
     */
    public static float getRate(int angleCount) {
        float outSizeAngle = (float) ((angleCount - 2) * Math.PI / angleCount);
        float insideAngle = (float) (Math.PI - outSizeAngle);
        return (float) (0.5 / Math.cos(insideAngle) * Math.sin(outSizeAngle / 2) * 2);
    }

    @Override
    public void draw() {
        vertexData.position(0);
        glVertexAttribPointer(aPositionLocation, POSITION_COMPONENT_COUNT, GL_FLOAT,
                false, STRIDE, vertexData);
        vertexData.position(POSITION_COMPONENT_COUNT);
        glVertexAttribPointer(aColorLocation, COLOR_COMPONENT_COUNT, GL_FLOAT,
                false, STRIDE, vertexData);

        glDrawArrays(GL_TRIANGLE_FAN, 0, pointCount);
    }
}

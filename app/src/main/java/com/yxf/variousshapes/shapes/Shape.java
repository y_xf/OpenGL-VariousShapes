package com.yxf.variousshapes.shapes;


import com.yxf.variousshapes.Point;

public abstract class Shape {
    public static final int BYTES_PER_FLOAT = 4;
    public static final int POSITION_COMPONENT_COUNT = 2;
    public static final int COLOR_COMPONENT_COUNT = 3;
    public static final int STRIDE = (POSITION_COMPONENT_COUNT +
            COLOR_COMPONENT_COUNT) * BYTES_PER_FLOAT;
    protected int aPositionLocation;
    protected int aColorLocation;

    protected Point center;
    protected float width;
    protected float height;

    public Shape(Point center, float width, float height) {
        this.center = center;
        this.width = width;
        this.height = height;
    }

    public void setLocation(int aPositionLocation, int aColorLocation) {
        this.aPositionLocation = aPositionLocation;
        this.aColorLocation = aColorLocation;
    }

    public abstract void draw();

}
